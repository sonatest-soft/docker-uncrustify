FROM alpine:latest
MAINTAINER Maxime Roussin-Bélanger "roussinm@sonatest.com"

ENV UNCRUSTIFY_VERSION 0.64

RUN apk upgrade --update

RUN apk add --no-cache make bash curl tar build-base cmake

RUN curl -fSL "https://github.com/uncrustify/uncrustify/archive/uncrustify-$UNCRUSTIFY_VERSION.tar.gz" -o uncrustify.tar.gz  \
	&& readonly NPROC=$(grep -c ^processor /proc/cpuinfo 2>/dev/null || echo 1) \
	&& echo "Using upto $NPROC threads" \
	&& mkdir -p /usr/src/uncrustify \
	&& tar -xf uncrustify.tar.gz -C /usr/src/uncrustify --strip-components=1 \
	&& rm uncrustify.tar.gz* \
	&& dir="$(mktemp -d)" \
	&& cd "$dir" \
	&& cmake /usr/src/uncrustify \
	&& make -j$NPROC \
	&& make install \
	&& rm -rf "$dir" \
	&& rm -rf /usr/src/uncrustify 
